package tfidf

type Document struct {
	fields map[string]*Field
}

func (d *Document) Fields() map[string]*Field {
	return d.fields
}

func (d *Document) Field(name string) *Field {
	field := d.fields[name]
	if field == nil {
		field = &Field{name}
		d.fields[name] = field
	}
	return field
}

func NewDocument() *Document {
	return &Document{make(map[string]*Field)}
}

type Field struct {
	name string
}

type Index struct {
	fields map[string]*IndexField
}

func NewIndex() *Index {
	return &Index{make(map[string]*IndexField)}
}

func (i *Index) Field(name string) *IndexField {
	indexField := i.fields[name]
	if indexField == nil {
		indexField = &IndexField{name}
		i.fields[name] = indexField
	}
	return indexField
}

func (i *Index) AddDocument(d Document) {
	for name,field := range(d.Fields()) {
		indexField := i.Field(name)
		indexField.AddDocumentField(field)
	}
}

type IndexField struct {
	name string
}

func (inf *IndexField) AddDocumentField(f *Field) {
}

